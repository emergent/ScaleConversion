+ Scale {

	*convert {|scale, newEDO, name|
		var oldStepsPerOctave, newDegrees, newScale;

		oldStepsPerOctave = scale.pitchesPerOctave;

		newDegrees = (scale.semitones * newEDO/oldStepsPerOctave).round(1.0);
		newScale = Scale.new(newDegrees, newEDO, Tuning.et(newEDO), name);
		^newScale;
	}
}
